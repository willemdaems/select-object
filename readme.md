Select object using chaining method for building up a query.
This is mainly used as a wrapper around PDO functions.

Supports toString function for outputting the query as is.