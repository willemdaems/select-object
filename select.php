<?php
/**
 * Select.php
 *
 * Query wrapper class for creating and updating tables.
 * Also stores table and column information
 *
 * @example
 *
 *  $select = new Select();
 *  $result = $select
 *      ->from('dash_modules')
 *      ->where('workflow', '1');
 *
 *  $result->where('deleted', 0);
 *
 *
 * @package Dash
 * @subpackage $db;
 * @todo Refactor __toString
 *
 */
namespace Db\Pdo;

use Dash\Application;

class Select
{
    /**
     * where clauses
     * @var array
     */
    private $where = array();

    /**
     * Joins
     * @var array
     */
    private $joins = array();

    /**
     * From table
     * @var string
     */
    private $from = '';

    /**
     * Fields to select
     * @var array
     */
    private $columns = '';

    /**
     * LIMIT clause
     * @var int
     */
    private $limit;

    /**
     * offset indicator for the LIMIT clause
     * @var int
     */
    private $offset;

    /**
     * Order clause
     * @var type
     */
    private $order = array();

    /**
     * Database instance
     * @var type
     */
    private $db;

    /**
     * Prepared statement collection of arguments
     * Given by where()
     * @var array
     */
    private $arguments;

    private $mysqlfunctions = array(
        'COUNT', 'AVG', 'MIN', 'MAX'
    );

    private $stmt;

    /**
     * Constructor, accepts a table
     * @param string $from
     * @return \Dash\Db\Mysqli\Select
     */
    public function __construct($from = '')
    {
        if (!empty($from)) {
            $this->from = $from;
        }

        $this->db = Application::getDatabase();

        return $this;
    }

    /**
     * Add from table
     * @param string $from
     * @return \Dash\Db\Mysqli\Select
     */
    public function from($from, $fields = array(), $db = '')
    {

        if (!empty($db)) {
            $this->from = "`" . $db . "`.";
        }

        $this->from .= "`" . $from . "`";

        if (empty($fields)) {
            $this->columns = '*';
        }

        if (!empty($fields)) {
            foreach ($fields as $i => $field) {

                // user can use the index as mixed type
                // an array could look like:
                // array(
                //  'id', 'title', 'text' => 'description'
                // )
                // php allows this type of mixed indexing
                if (gettype($i) === 'string') {
                    $this->addColumn($from, $i, $field);
                } else {
                    $this->addColumn($from, $field);
                }
            }
        }

        return $this;
    }

    private function isAgg($field)
    {
        foreach ($this->mysqlfunctions as $agg) {
            if (stripos($field, $agg) !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * Assigns data to the class columns variable. One can add an alias as a third
     * options parameter
     *
     * @example $this->addColumn('dash_table', 'columnname', 'columnalias')
     * @param string $table
     * @param string $field
     * @param string $alias
     * @return void
     */
    private function addColumn($table, $field, $alias = '')
    {
        if ($this->isAgg($field) === false) {
            $column = "`".$table."`" . '.';
        } else {
            $column = '';
        }

        if (!empty($alias)) {

            $column .=  $field. " AS `".$alias."`";
        } else {
            $column .= "`".$field."`";
        }

        if (empty($this->columns)) {
            $this->columns = $column;
        } else {
            $this->columns .= ", ". $column;
        }
    }

    /**
     * Adds a join ytpe to the class array. Also fetches the columns (supports aliasing)
     * and add them to the class columns variable
     *
     * @param string $type (natural | left)
     * @param string $table
     * @param string $condition
     * @param array $fields
     * @return void
     */
    private function addJoin($type, $table, $condition, $fields)
    {
        array_push($this->joins, array(
            'type' => $type,
            'join_table' => $table,
            'condition' => $condition,
            'fields' => $fields
        ));

        foreach ($fields as $i => $field) {
            if (gettype($i) === 'string') {
                $this->addColumn($table, $i, $field);
            } else {
                $this->addColumn($table, $field);
            }
        }
    }

    public function joinLeft($table, $condition, $fields)
    {
        $this->addJoin('left', $table, $condition, $fields);
        return $this;
    }

    public function join($reference, $table, $condition)
    {
        $this->addJoin('natural', $reference, $table, $condition);
        return $this;
    }

    /**
     * ORDER clause
     * @param type $clause
     */
    public function order($clause)
    {
        array_push($this->order, $clause);
        return $this;
    }

    /**
     * Add LIMIT clause with optional offset parameter
     * @param int $limit
     * @param int $offset
     */
    public function limit($limit, $offset = 0)
    {
        $this->limit = (int) $limit;
        $this->offset = (int) $offset;
        return $this;
    }

    /**
     * Add where clauses
     * @param string $field
     * @param string $argument
     * @return \Dash\Db\Mysqli\Select
     */
    public function where($field, $argument = '')
    {
        $field = trim($field);
        $placeholder = false;

        // ensure we encounter a placeholder
        if (preg_match("/=\s*\?$/", $field) === 1) {
            $placeholder = true;
        }

        array_push($this->where, array($field, $argument, $placeholder));
        return $this;
    }

    /**
     * Fetches first record in resultst
     * @return array | bool
     */
    public function fetchRow()
    {
        $this->execute();
        return $this->stmt->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Fetches first record in resultst
     * @return array | bool
     */
    public function fetchObj()
    {
        $this->execute();
        return $this->stmt->fetch(\PDO::FETCH_OBJ);
    }

    /**
     * Fetch all rows in the resultset
     * @return array | bool
     */
    public function fetchAll()
    {
        $this->execute();
        return $this->stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Put the query parts together
     *
     * @todo better error catching. There are unsafe operations here.
     * @todo use the toString function or only use this function
     * @return string
     */
    public function execute()
    {
        // start width SELECT and adding the defined columns
        $sql = 'SELECT '.$this->columns.' FROM ';
        $sql .= $this->from;

        // empty array for binding variables to placeholders
        $binds = array();

        // empty WHERE clause. This will be user later
        $where = '';

        // loop through the added joins
        if (!empty($this->joins)) {
            foreach ($this->joins as $join) {
                switch ( $join['type'] ) {
                    case 'left':
                        $joinType = ' LEFT ';
                        break;
                    case 'natural':
                        $joinType = '';
                        break;
                    default:
                        $joinType = '';
                        break;

                }

                $sql .= $joinType . ' JOIN ' . $join['join_table'] . ' ON ' . $join['condition'];
            }
        }

        // loop through the where array. Users can add
        // one or more where() clauses
        foreach ($this->where as $array) {

            // $array[2] evaluates to true if a placeholder is discovered
            if ($array[2] === true) {
                array_push($binds, $array[1]);
            }

            // continue build up the sql
            if (empty($where)) {
                $keyword = ' WHERE ';
            } else {
                $keyword = ' AND ';

            }

            $where .= $keyword .$array[0];
        }

        // add where clauses to the sql
        $sql .= $where;

        // loop through the order by clauses
        if (!empty($this->order)) {
            $sql .= " ORDER BY ";
            foreach($this->order as $order) {
                $sql .= $order . ', ';
            }

            $sql = rtrim(trim($sql), ',');
        }

        if (!empty($this->limit)) {

            $sql .= " LIMIT ";

            if (!empty($this->offset)) {
                $sql .= " " . (int) $this->offset . ", ";
            }

            $sql .= " " . (int) $this->limit;
        }

        // create a new prepared statement
        $this->stmt = $this->db->prepare($sql);

        // bind the gathered variables
        $this->stmt->execute($binds);

        // return the sql as string
        return $sql;
    }

    /**
     * Output the query as string
     *
     * @todo split up functionality
     * @return string
     */
    public function __toString()
    {
        // start width SELECT and adding the defined columns
        $sql = 'SELECT '.$this->columns.' FROM ';
        $sql .= $this->from;

        // empty array for binding variables to placeholders
        $binds = array();

        // empty WHERE clause. This will be user later
        $where = '';

        // loop through the added joins
        if (!empty($this->joins)) {
            foreach ($this->joins as $join) {
                switch ( $join['type'] ) {
                    case 'left':
                        $joinType = 'LEFT';
                        break;
                    case 'natural':
                        $joinType = '';
                        break;
                    default:
                        $joinType = '';
                        break;

                }

                $sql .= $joinType . ' JOIN ' . $join['join_table'] . ' ON ' . $join['condition'];
            }
        }

        // loop through the where array. Users can add
        // one or more where() clauses
        foreach ($this->where as $array) {

            // $array[2] evaluates to true if a placeholder is discovered
            if ($array[2] === true) {
                array_push($binds, $array[1]);
            }

            // continue build up the sql
            if (empty($where)) {
                $keyword = ' WHERE ';
            } else {
                $keyword = ' AND ';

            }

            $where .= $keyword .$array[0];
        }

        // add where clauses to the sql
        $sql .= $where;

        // loop through the order by clauses
        if (!empty($this->order)) {
            $sql .= " ORDER BY ";
            foreach($this->order as $order) {
                $sql .= $order . ', ';
            }

            $sql = rtrim(trim($sql), ',');
        }

        if (!empty($this->limit)) {

            $sql .= " LIMIT ";

            if (!empty($this->offset)) {
                $sql .= " " . (int) $this->offset . ", ";
            }

            $sql .= " " . (int) $this->limit;
        }

        // return the sql as string
        return $sql;
    }
}
